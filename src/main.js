navigator.serviceWorker.register('js/sw.js');
var icurating = require('icu-rating');
var level = require('level-browserify')
var db = level('./mydb')

window.games = [];
window.kFactor = 24;
window.userrating = 1600;

function save(callback) {
    var data = {
        games : window.games,
        kFactor : window.kFactor,
        userrating : window.userrating
    }
    db.put('tournament', JSON.stringify(data), function (err) {
        if(err) {
            console.log('Error creating the database');
            console.log(err);
        }
        callback();
    });
}
db.get('tournament', function (err, data) {
    if (err) {
        save(function() {
            calcrating();
            renderTable();
        })
        return;
    }
    value = JSON.parse(data);
    window.games = value.games;
    window.userrating = value.userrating;
    window.kFactor = value.kFactor;
    document.getElementById('txtKFactor').value = value.kFactor;
    document.getElementById('txtCurrentRating').value = value.userrating;

    calcrating(window.userrating, window.games, window.kFactor);
    renderTable();
})

var btnAdd = document.getElementById('btnAdd');
btnAdd.addEventListener('click', function(el) {
    var res = document.getElementById('txtResult').value;
    var oppRating = document.getElementById('opponentsRating').value;
    var resVal = convertresult(res);
    var game = {
        rating : parseInt(oppRating),
        result : resVal
    };
    window.games.push(game);

    save(function() {
        renderTable();
        calcrating(window.userrating, window.games, window.kFactor);    
    })
})

var txtRating = document.getElementById('txtCurrentRating');
txtRating.addEventListener('input', function(e) {
    window.userrating = parseInt(txtRating.value);
    
    save(function() {
    
        renderTable();
        calcrating(window.userrating, window.games, window.kFactor);    
        console.log(window.userrating)
    })
});

var txtKFactor = document.getElementById('txtKFactor');
txtKFactor.addEventListener('input', function(e) {
    window.kFactor = parseInt(txtKFactor.value);
    
    save(function() {
        renderTable();
        calcrating(window.userrating, window.games, window.kFactor);    
        console.log(txtKFactor.value)
    })
});


function calcrating(userrating, games, kFactor) {
    var output = icurating(userrating, games, kFactor);
    document.getElementById('newRating').innerHTML = "New Rating: " + output.newRating;
}

function renderTable() {
    var tbl = document.getElementById('opponentsTable');
    tbl.innerHTML = '';
    for(var gamecounter=0; gamecounter < games.length; gamecounter++) {
        var row = tbl.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        cell1.innerHTML = games[gamecounter].rating;
        cell2.innerHTML = convertresult(games[gamecounter].result);
        var btn = document.createElement("button");
        btn.appendChild(document.createTextNode("Remove"));
        btn.setAttribute('class', 'button-primary');
        btn.setAttribute('data-gameindex', gamecounter)
        btn.addEventListener('click', function(e) {
            window.games.splice(this.dataset.gameindex, 1); 
            save(function(){
                renderTable();
                calcrating(window.userrating, window.games, window.kFactor);
            })
        });
        
        cell3.appendChild(btn);
    }
}

function convertresult(result) {
    switch(result) {
        case 1 :
            return "Win"
        case 0.5 :
            return "Draw"
        case -1 :
            return "Loss"
        case "win" :
            return 1;
        case "draw" :
            return 0.5;
        case "loss" :
            return -1;
    }
}
